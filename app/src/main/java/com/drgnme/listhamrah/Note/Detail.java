package com.drgnme.listhamrah.Note;

import java.util.UUID;

/**
 * Created by DRGNme on 7/21/2017.
 */

public class Detail {

    private UUID mId;
    private String mTitle;
    private String mDes;
    private String mDate;
    private boolean mDone;
//    private String idC;

    public Detail() {
        this(UUID.randomUUID());
    }
/*

    public String getIdC() {
        return idC;
    }

    public void setIdC(String idC) {
        this.idC = idC;
    }
*/

    public Detail(UUID id) {
        mId = id;
       // mDate = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDes() {
        return mDes;
    }

    public void setDes(String des) {
        mDes = des;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public boolean isDone() {
        return mDone;
    }

    public void setDone(boolean done) {
        mDone = done;
    }

    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }

}
