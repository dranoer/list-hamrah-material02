package com.drgnme.listhamrah.Note.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.drgnme.listhamrah.Note.Detail;
import com.drgnme.listhamrah.customcalendar.core.models.Day;

import java.util.UUID;

/**
 * Created by DRGNme on 7/29/2017.
 */

public class DetailCursorWrapper extends CursorWrapper {
    public DetailCursorWrapper (Cursor cursor) {
        super(cursor);
    }

    public Detail getDetail() {
        String uuidString = getString(getColumnIndex(DetailDbSchema.DetailTable.Cols.UUID));

        String title = getString(getColumnIndex(DetailDbSchema.DetailTable.Cols.TITLE));

        String des = getString(getColumnIndex(DetailDbSchema.DetailTable.Cols.DES));

        String date = getString(getColumnIndex(DetailDbSchema.DetailTable.Cols.DATE));

//        float getRate = getFloat(getColumnIndex
//                (DetailDbSchema.DetailTable.Cols.RATE));

        int isDone = getInt(getColumnIndex(DetailDbSchema.DetailTable.Cols.DONE));

//        String idc = getString(getColumnIndex(DetailDbSchema.DetailTable.Cols.IDC));

        Detail detail = new Detail(UUID.fromString(uuidString));
        detail.setTitle(title);
        detail.setDes(des);
        detail.setDate(date);
//        detail.setRate(getRate);
        detail.setDone(isDone != 0);
//        detail.setIdC(idc);

        return detail;
//        return null;
    }

    public Day getCalendar() {
        Boolean longpress = Boolean.getBoolean(DetailDbSchema.CalendarTable.ColCalendar.LONGPRESS);

        Day day = new Day();
        day.setLongpress(longpress);
        return day;
    }

}
