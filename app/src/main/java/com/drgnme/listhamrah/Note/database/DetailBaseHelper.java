package com.drgnme.listhamrah.Note.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by DRGNme on 7/29/2017.
 */

public class DetailBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "detailBase.db";

    public DetailBaseHelper (Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate (SQLiteDatabase db) {
        db.execSQL("create table " + DetailDbSchema.DetailTable.NAME +
                "(" +
                " _id integer primary key autoincrement," +
                DetailDbSchema.DetailTable.Cols.UUID + ", " +
                DetailDbSchema.DetailTable.Cols.TITLE + ", " +
                DetailDbSchema.DetailTable.Cols.DES + ", " +
                DetailDbSchema.DetailTable.Cols.DATE + ", " +
                DetailDbSchema.DetailTable.Cols.DONE +
//                ", " +
//                DetailTable.Cols.IDC +
                ")"
        );

        db.execSQL("create table " + DetailDbSchema.CalendarTable.NAME +
        "(" +
        "_id integer primary key autoincrement," +
                DetailDbSchema.CalendarTable.ColCalendar.LONGPRESS + ")");
    }

    @Override
    public void onUpgrade (SQLiteDatabase db,
                           int oldVersion, int newVersion) {

    }

}
