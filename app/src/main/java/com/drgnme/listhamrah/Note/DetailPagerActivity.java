package com.drgnme.listhamrah.Note;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.drgnme.listhamrah.R;

import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by DRGNme on 7/24/2017.
 */

public class DetailPagerActivity extends AppCompatActivity {
    private static final String EXTRA_DETAIL_ID = "com.drgnme.listhamrah.detail_id";

    @BindView(R.id.detail_view_pager)
    ViewPager mViewPager;
    private List<Detail> mDetails;

    public static Intent newIntent(Context packageContext, UUID detailId) {
        Intent intent = new Intent(packageContext, DetailPagerActivity.class);
        intent.putExtra(EXTRA_DETAIL_ID, detailId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pager);
        ButterKnife.bind(this);

        //mDetails = DetailLab.get(this).getDetails();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                UUID detailId = (UUID) getIntent().getSerializableExtra(EXTRA_DETAIL_ID);
                return DetailFragment.newInstance(detailId);
            }

            @Override
            public int getCount() {
                return 1;
            }
        });
    }

}

