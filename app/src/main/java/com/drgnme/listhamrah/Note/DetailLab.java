package com.drgnme.listhamrah.Note;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drgnme.listhamrah.Note.database.DetailBaseHelper;
import com.drgnme.listhamrah.Note.database.DetailCursorWrapper;
import com.drgnme.listhamrah.Note.database.DetailDbSchema;
import com.drgnme.listhamrah.customcalendar.core.models.Day;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DetailLab {
    private static DetailLab sDetailLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static DetailLab get(Context context) {
        if (sDetailLab == null) {
            sDetailLab = new DetailLab(context);
        }
        return sDetailLab;
    }

    /// Constructor
    public DetailLab(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new DetailBaseHelper(mContext).getWritableDatabase();
    }

    public List<Detail> getDetails() {
        List<Detail> details = new ArrayList<>();
        DetailCursorWrapper cursor = queryDetails(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                details.add(cursor.getDetail());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        if(details.size() == 0)
        {
        }

        return details;
    }


    public List<Detail> getDetail(String day) {

        List<Detail> details=new ArrayList<>();
        DetailCursorWrapper cursorWrapper = queryDate(DetailDbSchema.DetailTable.Cols.DATE + " = " + day);

        try {
            cursorWrapper.moveToFirst();
            while (!cursorWrapper.isAfterLast())
            {
                details.add(cursorWrapper.getDetail());
                cursorWrapper.moveToNext();
            }
        }finally {
            cursorWrapper.close();
        }

        if(details.size() == 0)
        {
        }

        return details;
    }

    public void addDetail(Detail d) {
        ContentValues values = getContentValues(d);
        mDatabase.insert(DetailDbSchema.DetailTable.NAME, null, values);
    }

    /////
    public void addCalendar(Day day) {
        ContentValues values = getContentValuesCalendar(day);
        mDatabase.insert(DetailDbSchema.CalendarTable.NAME, null, values);
    }

    public void deleteDetail(Detail detail) {

        mDatabase.delete(DetailDbSchema.DetailTable.NAME,
                DetailDbSchema.DetailTable.Cols.UUID + " = ?",
                new String[]{detail.getId().toString()});
    }

    public void deleteAllDetail() {
        mDatabase.delete(DetailDbSchema.DetailTable.NAME, null, null);
    }

    public Detail getDetail(UUID id) {
        DetailCursorWrapper cursor = queryDetails
                (DetailDbSchema.DetailTable.Cols.UUID + " = ?",
                        new String[]{id.toString()});
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getDetail();
        } finally {
            cursor.close();
        }
    }

    public Detail getDayDetail(String day) {
        DetailCursorWrapper cursor = queryDetails
                (DetailDbSchema.DetailTable.Cols.DATE + " = ?",
                        new String[]{day.toString()});
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getDetail();
        } finally {
            cursor.close();
        }
    }

    public File getPhotoFile(Detail detail) {
        File fileDir = mContext.getFilesDir();
        return new File(fileDir, detail.getPhotoFilename());
    }

    public void updateDetail(Detail detail) {
        String uuidString = detail.getId().toString();
        ContentValues values = getContentValues(detail);

        mDatabase.update(DetailDbSchema.DetailTable.NAME, values,
                DetailDbSchema.DetailTable.Cols.UUID + " = ?",
                new String[]{uuidString});
    }

    public DetailCursorWrapper queryDetails
    (String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                DetailDbSchema.DetailTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new DetailCursorWrapper(cursor);
    }

    public DetailCursorWrapper queryDate(String whereClause)
    {
        Cursor cursor =mDatabase.query(DetailDbSchema.DetailTable.NAME,null,whereClause,null,null,null,null,null);
        return new DetailCursorWrapper(cursor);
    }

    private static ContentValues getContentValues(Detail detail) {

        ContentValues values = new ContentValues();
        values.put(DetailDbSchema.DetailTable.Cols.UUID, detail.getId().toString());
        values.put(DetailDbSchema.DetailTable.Cols.TITLE, detail.getTitle());
        values.put(DetailDbSchema.DetailTable.Cols.DES, detail.getDes());
        values.put(DetailDbSchema.DetailTable.Cols.DATE, detail.getDate());
//        values.put(DetailTable.Cols.RATE, detail.getRate());
        values.put(DetailDbSchema.DetailTable.Cols.DONE, detail.isDone() ? 1 : 0 );

        return values;
    }

    private static ContentValues getContentValuesCalendar(Day d) {
        ContentValues values = new ContentValues();
        values.put(DetailDbSchema.CalendarTable.ColCalendar.LONGPRESS, d.isLongpress());

        return values;
    }

}
