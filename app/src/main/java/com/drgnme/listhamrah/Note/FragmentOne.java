package com.drgnme.listhamrah.Note;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.drgnme.listhamrah.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FragmentOne extends Fragment {
    private static final String SAVED_NUMBER_VISIBLE = "number";
    private static final String EXTRA_DETAIL_ID = "com.drgnme.listhamrah.detail_id";

/*    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView centerTitle;*/
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.detail_recycler_view)
    RecyclerView mDetailRecyclerView;
    private Unbinder unbinder;

    private DetailAdapter mAdapter;
    private boolean mNumberVisible;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
//        initToolbar();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true); //This will reverse the data order but not scroll the RecyclerView to the last item
        layoutManager.setStackFromEnd(true); //For keeping data order same and simply scrolling the RecyclerView to the last item
        mDetailRecyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration
                (mDetailRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        mDetailRecyclerView.addItemDecoration(dividerItemDecoration);

        if (savedInstanceState != null) {
            mNumberVisible = savedInstanceState.getBoolean(SAVED_NUMBER_VISIBLE);
        }

        /////////// change the fab color in normal state
        int darkColorValue = Color.parseColor("#2b3f62");
        fab.setBackgroundTintList(ColorStateList.valueOf(darkColorValue));

        ///////// change the fab color in pressed state
        int color = Color.parseColor("#008ab8");
        fab.setRippleColor(color);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Detail detail = new Detail();
        //        DetailLab.get(getActivity()).addDetail(detail);
                Intent intent = DetailPagerActivity.newIntent(getActivity(), null);
                startActivity(intent);
            }
        });

        initViews();
        updateUI();
        return view;
    }

/*
    public void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar();
        centerTitle.setText("یادداشت");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.icon_back_smaller_light);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }
*/

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_NUMBER_VISIBLE, mNumberVisible);
    }

    public void updateUI() {
        DetailLab detailLab = DetailLab.get(getActivity());
        List<Detail> details = detailLab.getDetails();
        if (details.size() == 0) {
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.empty_view);
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.empty_view);
            linearLayout.setVisibility(View.GONE);
        }

        if (mAdapter == null) {
            mAdapter = new DetailAdapter(details);
            mDetailRecyclerView.setAdapter(mAdapter);

        } else {
            mAdapter.setDetails(details);
            mAdapter.notifyDataSetChanged();
        }
    }

    private class DetailHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        private TextView mTitleTextView;
        private TextView mDescriptions;
        private Detail mDetail;
        private RatingBar mRatingBar;
        private View mDoneButton, mUndoneBotton;
        private ImageView mReportButton;
        private ImageView share;

        public DetailHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_detail,
                    parent, false));

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            mTitleTextView = (TextView) itemView.findViewById(R.id.detail_title);
            mDescriptions = (TextView) itemView.findViewById(R.id.detail_descriptions);
            mDoneButton = (ImageView) itemView.findViewById(R.id.detail_list_done);
            mUndoneBotton = (ImageView) itemView.findViewById(R.id.detail_list_undone);
            mReportButton = (ImageView) itemView.findViewById(R.id.detail_list_share);
            share = (ImageView) itemView.findViewById(R.id.detail_list_share);
        }

        public void bind(Detail detail) {
            mDetail = detail;
            mTitleTextView.setText(mDetail.getTitle());
            mDescriptions.setText(mDetail.getDes());
            mDoneButton.setVisibility(detail.isDone() ? View.VISIBLE : View.INVISIBLE);
            mUndoneBotton.setVisibility(detail.isDone() ? View.INVISIBLE : View.VISIBLE);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), getAdapterPosition() + "", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onClick(View view) {
            Intent intent = DetailPagerActivity.newIntent(getActivity(), mDetail.getId());
            startActivity(intent);
        }


        @Override
        public boolean onLongClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    getActivity());
            alert.setMessage(R.string.alert);
            alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    DetailLab.get(getActivity()).deleteDetail(mDetail);
                    updateUI();
                    dialog.dismiss();
                }
            });
            alert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
            return true;
        }
    }

    private class DetailAdapter extends RecyclerView.Adapter<DetailHolder> {
        private List<Detail> mDetails;
        private Detail mDetail;

        public DetailAdapter(List<Detail> details) {
            mDetails = details;
        }

        @Override
        public DetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new DetailHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(DetailHolder holder, int position) {
            Detail detail = mDetails.get(position);
            holder.bind(detail);
        }

        @Override
        public int getItemCount() {
            return mDetails.size();
        }

        public void setDetails(final List<Detail> details) {
            mDetails = details;
        }

        public void removeItem(int position) {
            mDetail = mDetails.get(position);
            DetailLab.get(getActivity()).deleteDetail(mDetail);
            updateUI();
        }

        public void removeItemView(int position) {
            notifyItemChanged(position);
        }
    }

    public void initViews() {
        mDetailRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}