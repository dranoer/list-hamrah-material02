package com.drgnme.listhamrah.Note;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.drgnme.listhamrah.R;

/**
 * Created by DRGNme on 7/31/2017.
 */

public class PhotoZoomFragment extends DialogFragment {

    private static final String ARG_IMG = "image";
    private ImageView mImageZoom;

    public static PhotoZoomFragment newInstance (String imageFile) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_IMG, imageFile);

        PhotoZoomFragment fragment = new PhotoZoomFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        String imageFile = (String) getArguments().getSerializable(ARG_IMG);
        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_photo, null);

        Bitmap bitmap = PictureUtils.getScaledBitmap(imageFile, getActivity());

        mImageZoom = (ImageView) v.findViewById(R.id.dialog_photo_zoom);
        mImageZoom.setImageBitmap(bitmap);

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .create();
    }

}
