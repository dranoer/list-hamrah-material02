package com.drgnme.listhamrah.Note;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by DRGNme on 9/1/2017.
 */
public class NoteEditText extends AppCompatEditText {
    private Rect mRect;
    private Paint mPaint;
    private Typeface typeFace;

    public NoteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.parseColor("#C0C0C0")); //SET YOUR OWN COLOR HERE
    }

    public void init(Context context) {
        typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/yekan.TTF");
        this.setTypeface(typeFace);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        super.setTypeface(typeFace, style);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(typeFace);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int height = getHeight();
        int line_height = getLineHeight();
        int count = height / line_height;

        if (getLineCount() > count)
            count = getLineCount();//for long text with scrolling

        Rect r = mRect;
        Paint paint = mPaint;
        int baseline = getLineBounds(0, r);//first line

        for (int i = 0; i < count; i++) {

            canvas.drawLine(r.left, baseline + 1, r.right, baseline + 1, paint);
            baseline += getLineHeight();//next line
        }

        super.onDraw(canvas);
    }
}
