package com.drgnme.listhamrah.Note;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.drgnme.listhamrah.R;
import com.drgnme.listhamrah.customcalendar.SpManager;
import com.drgnme.listhamrah.customcalendar.core.models.Day;
import com.drgnme.listhamrah.utility.TypefaceSpan;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by DRGNme on 7/21/2017.
 */

public class DetailFragment extends Fragment {

    public static final String ARG_DETAIL_ID = "detail_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final String DIALOG_PHOTO = "DialogPhoto";
    private static final String TAG = "tag";

    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_PHOTO = 2;

    @BindView(R.id.detail_delete)
    ImageView mDelete;
    @BindView(R.id.save_note)
    ImageView mCheck;
    @BindView(R.id.detail_back)
    ImageView mBack;
    @BindView(R.id.detail_report)
    ImageView mReportButton;
    @BindView(R.id.detail_title)
    EditText mTitleField;
    @BindView(R.id.detail_description)
    EditText mDescriptionField;
    @BindView(R.id.detail_done)
    CheckBox mDoneButton;
    @BindView(R.id.detail_date)
    TextView mDate;
    private Unbinder unbinder;

    private Detail mDetail;
    private File mPhotoFile;
    private Button mDateButton;
    private ImageView mPhotoView;
    private String dayOfYear;
    private UUID detailId;

    private IDetail iDetail;
    private SpManager spManager;

    public static DetailFragment newInstance (UUID detailId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DETAIL_ID, detailId);

        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailId = (UUID) getArguments().getSerializable(ARG_DETAIL_ID);
        if (detailId != null)
        mDetail = DetailLab.get(getActivity()).getDetail(detailId);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        unbinder = ButterKnife.bind(this, v);
        dayOfYear = getActivity().getIntent().getStringExtra("dayOfYear");


        spManager = SpManager.getInstance(getActivity());
        initView();

        return v;
    }


    public void initView() {

        if (mDetail != null) {
            mTitleField.setText(mDetail.getTitle());

            mTitleField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    mDetail = new Detail(detailId);
                    mDetail.setTitle(s.toString());
                    mDetail.setDes(mDescriptionField.getText().toString());
//                    mDetail.setDone(mDoneButton.isChecked());
                    mDetail.setDone(true);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        if (mDetail != null) {
            mDescriptionField.setText(mDetail.getDes());

            mDescriptionField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mDetail.setDes(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }


        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (mDetail != null) {
                    DetailLab.get(getActivity()).deleteDetail(mDetail);
                    getActivity().finish();
                }
                else {
                    Toast.makeText(getContext(), "اطلاعاتی برای حذف وجود ندارد", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (isEmpty(mTitleField)) {
                    Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.TTF");
                    SpannableString efr = new SpannableString("عنوان یادداشت نمی تواند خالی باشد");
                    efr.setSpan(new TypefaceSpan(font), 0, efr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    Toast.makeText(getContext(), efr, Toast.LENGTH_SHORT).show();
                } else if (isEmpty(mDescriptionField)) {
                    Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.TTF");
                    SpannableString efr = new SpannableString("متن یادداشت نمی تواند خالی باشد");
                    efr.setSpan(new TypefaceSpan(font), 0, efr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    Toast.makeText(getContext(), efr, Toast.LENGTH_SHORT).show();
                } else {
                    if (detailId != null)
                    DetailLab.get(getActivity()).updateDetail(mDetail);
                    else{
                        detailId = UUID.randomUUID();
                        mDetail = new Detail(detailId);
                        Day day=new Day();
                        day.setLongpress(true);
                        mDetail.setTitle(mTitleField.getText().toString());
                        mDetail.setDes(mDescriptionField.getText().toString());
                        mDetail.setDate(dayOfYear);
                        DetailLab.get(getActivity()).addDetail(mDetail);
                        DetailLab.get(getActivity()).addCalendar(day);
                    }

                    getActivity().finish();
                }
            }
        });

        if (mDetail != null) {
//            String fullDate = mDetail.getDate();
//            fullDate.trim();
//            fullDate.getChars("", "", "", "");
            mDate.setText(mDetail.getDate());

        }else mDate.setText("no date");

        mBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick (View v) {
                        getActivity().finish();
                    }
                });

        mReportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick (View v) {
                if (mDetail != null) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_TEXT, getDetailReport());
                    i.putExtra(Intent.EXTRA_SUBJECT,
                            getString(R.string.detail_report_subject));
                    i = Intent.createChooser(i, getString(R.string.send_report));
                    startActivity(i);
                } else {
                    Toast.makeText(getContext(), "اطلاعاتی برای اشتراک گذاری وجود ندارد", Toast.LENGTH_SHORT).show();
                }

            }
        });


        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.TTF");
        mDoneButton.setTypeface(face);
        if (mDetail != null) {
            mDoneButton.setChecked(mDetail.isDone());
            mDoneButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mDetail.setDone(isChecked);
                }
            });
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            if (requestCode == REQUEST_DATE) {
               // Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);

                updateDate();
            } else if (requestCode == REQUEST_PHOTO) {
                Uri uri = FileProvider.getUriForFile(getActivity(),"com.drgnme.listhamrah.fileprovider", mPhotoFile);
                getActivity().revokeUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        }

    private void updateDate() {
        mDateButton.setText(
                new SimpleDateFormat("MM/dd/yyyy").format(mDetail.getDate()));
    }

    //////////////////////////////////// REPORT
    private String getDetailReport() {
        String doneString = null;
        if (mDetail.isDone()) {
            doneString = getString(R.string.crime_report_solved);
        } else {
            doneString = getString(R.string.crime_report_unsolved);
        }

        /*String dateFormat = "EEE, MMM dd";
        String dateString = DateFormat.format(
                dateFormat, mDetail.getDate()).toString();*/

        String report = getString(
                R.string.detail_report,
                mDetail.getTitle(),
                mDetail.getDes(),
                mDetail.getDate()
                );

        return report;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
