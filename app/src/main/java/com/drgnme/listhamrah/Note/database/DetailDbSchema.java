package com.drgnme.listhamrah.Note.database;

/**
 * Created by DRGNme on 7/29/2017.
 */

public class DetailDbSchema {

    public static final class DetailTable {
        public static final String NAME = "details";

        public static final class Cols
//                implements BaseColumns
        {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DES = "des";
            public static final String DATE = "date";
//            public static final String RATE = "rate";
            public static final String DONE = "done";
//            public static final String IDC = "idc";
        }
    }

    public static final class CalendarTable {
        public static final String NAME = "calendar";

        public static final class ColCalendar {

            public static final String LONGPRESS = "longpress";
        }
    }

}
