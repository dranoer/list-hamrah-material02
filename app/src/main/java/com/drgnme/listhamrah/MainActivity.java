package com.drgnme.listhamrah;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.drgnme.listhamrah.Note.Detail;
import com.drgnme.listhamrah.Note.DetailLab;
import com.drgnme.listhamrah.Note.FragmentOne;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_tab_note,
            R.drawable.ic_tab_calendar
    };

    private static long back_pressed;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //////// TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        ///////// DRAWER
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView =
                (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener
                (new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {

                            case R.id.nav_item_two:


                                AlertDialog.Builder alert_delete = new AlertDialog.Builder(
                                        MainActivity.this);
                                alert_delete.setMessage(R.string.alert_delete_all);
                                alert_delete.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        DetailLab detailLab = DetailLab.get(getApplicationContext());
                                        List<Detail> details = detailLab.getDetails();
                                        if (details.size() == 0) { //check for note exist !
                                            AlertDialog.Builder alert_empty = new AlertDialog.Builder(MainActivity.this);
                                            alert_empty.setMessage(R.string.alert_delete_all_empty);
                                            alert_empty.setNegativeButton(R.string.okay, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            alert_empty.show();
                                        } else {

                                            DetailLab.get(getApplicationContext()).deleteAllDetail();
                                            Fragment page = getSupportFragmentManager()
                                                    .findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.getCurrentItem());
                                            if (viewPager.getCurrentItem() == 0 && page != null) {
                                                ((FragmentOne) page).updateUI();
                                            }
                                        }
                                        dialog.dismiss();
                                    }
                                });
                                alert_delete.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alert_delete.show();
                                return false;

                            case R.id.nav_item_three:
                                AlertDialog.Builder alertHelp = new AlertDialog.Builder(
                                        MainActivity.this);
                                alertHelp.setMessage(R.string.help);
                                alertHelp.show();
                                return false;

                            case R.id.nav_item_eight:
                                AlertDialog.Builder alert = new AlertDialog.Builder(
                                        MainActivity.this);
                                alert.setIcon(R.drawable.ic_menu_send);
                                alert.setTitle(R.string.contact_title);
//                                alert.setMessage(R.string.contact);

//                                alert.setMessage("Insert your cool string with links and stuff here");
//                                Linkify.addLinks((TextView) alert.findViewById(android.R.id.message), Linkify.ALL);

                                TextView textView = new TextView(MainActivity.this);
                                textView.setMovementMethod(LinkMovementMethod.getInstance());
                                textView.setText(R.string.contact);
                                alert.setView(textView);

//                                alert.setMessage(Html.fromHtml("<a href=\"http://telegram.me/dranoer\">Telegram</a>"));
//                                        .create();
                                alert.setNegativeButton(R.string.okay, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                                alert.create();
                                alert.show();

//                                ((TextView)alert.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

                                return false;

                            case R.id.nav_item_nine:
                                onBackPressedNavigation();
                                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                                    mDrawerLayout.closeDrawers();
                                }

                            default:
                                return false;
                        }
                    }
                });

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentOne(), "ONE");
        adapter.addFragment(new CalendarFragmentLocal(), "TWO");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (back_pressed >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.exit_with_back_button, Toast.LENGTH_SHORT).show();
            back_pressed++;
        }
    }

    public void onBackPressedNavigation() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}

