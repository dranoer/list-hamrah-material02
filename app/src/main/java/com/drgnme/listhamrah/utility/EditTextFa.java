package com.drgnme.listhamrah.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;


/**
 * Created by afkari on 12/4/17.
 */

public class EditTextFa extends AppCompatEditText {

    private Typeface typeFace;
    private Context context;

    public EditTextFa(Context context) {
        super(context);
        init(context);
    }

    public EditTextFa(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EditTextFa(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/yekan.TTF");
        this.setTypeface(typeFace);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        super.setTypeface(typeFace, style);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(typeFace);
    }

}
