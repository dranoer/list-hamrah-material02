package com.drgnme.listhamrah.utility;

import android.app.Application;

import com.drgnme.listhamrah.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by DRGNme on 9/3/2017.
 */

public class PersianFonts extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/yekan.TTF")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
