package com.drgnme.listhamrah.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


/**
 * Created by afkari on 12/4/17.
 */

public class FaTextView extends android.support.v7.widget.AppCompatTextView {

    private Typeface typeFace;
    private Context context;

    public FaTextView(Context context) {
        super(context);
        init(context);
    }

    public FaTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FaTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/yekan.TTF");
        this.setTypeface(typeFace);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        super.setTypeface(typeFace, style);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(typeFace);
    }

}
