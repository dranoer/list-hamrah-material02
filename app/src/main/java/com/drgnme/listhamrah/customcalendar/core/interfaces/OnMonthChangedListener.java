package com.drgnme.listhamrah.customcalendar.core.interfaces;

import com.drgnme.listhamrah.customcalendar.core.models.PersianDate;

/**
 * Created by MADNESS on 3/23/2017.
 */

public interface OnMonthChangedListener {
    void onChanged(PersianDate date);
}
