package com.drgnme.listhamrah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.drgnme.listhamrah.Note.Detail;
import com.drgnme.listhamrah.Note.DetailLab;
import com.drgnme.listhamrah.Note.DetailPagerActivity;
import com.drgnme.listhamrah.customcalendar.PersianCalendarView;
import com.drgnme.listhamrah.customcalendar.SpManager;
import com.drgnme.listhamrah.customcalendar.core.PersianCalendarHandler;
import com.drgnme.listhamrah.customcalendar.core.interfaces.OnDayClickedListener;
import com.drgnme.listhamrah.customcalendar.core.interfaces.OnDayLongClickedListener;
import com.drgnme.listhamrah.customcalendar.core.interfaces.OnMonthChangedListener;
import com.drgnme.listhamrah.customcalendar.core.models.PersianDate;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by AndroidDev2 on 10/30/2017.
 */

public class CalendarFragmentLocal extends Fragment {

//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
//    @BindView(R.id.toolbar_title)
//    TextView centerTitle;
    @BindView(R.id.persian_calendar)
    PersianCalendarView pd;
    @BindView(R.id.AppCompatTextView1)
    TextView txtDayevent;
    @BindView(R.id.monthsnameandyears)
    TextView monthandyear;
    @BindView(R.id.calendar_today)
    TextView todayImageView;
    @BindView(R.id.calendar_header_left)
    TextView leftMonth;
    @BindView(R.id.calendar_header_right)
    TextView rightMonth;
    @BindView(R.id.go_today)
    ImageView mGoToday;
    @BindView(R.id.calendar_note_title)
    TextView notePreviewTitle;
    @BindView(R.id.calendar_note_des)
    TextView notePreviewDes;
    @BindView(R.id.note_cardview)
    CardView noteCardview;
    private Unbinder unbinder;

    PersianCalendarHandler calendarHandler, p;
    PersianDate today;
    View view;
    private static Detail mDetail;
    public static String noteDayOfMonth, noteMonth, noteYear, totalDay;
    private SpManager spManager;
    public static UUID currentId;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_calendar_local, container, false);
        unbinder = ButterKnife.bind(this, view);
        spManager = SpManager.getInstance(getActivity());

        setHasOptionsMenu(true);
//        initToolbar();

        final String PREFS_NAME = "MyPrefsFile9";
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        boolean dialogShown = settings.getBoolean("dialogShown", false);
        final Context context = getContext();

        /*if (!dialogShown) {
            AlertDialog dialog = new AlertDialog.Builder(context).setMessage(R.string.dialog_calendar_des).show();
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);
            Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.TTF");
            textView.setTypeface(face);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("dialogShown", true);
            editor.commit();
        }*/

        calendarHandler = pd.getCalendar();
        today = calendarHandler.getToday();


        if (!calendarHandler.getAllEventsForDay(today).isEmpty()) {
            String ss = calendarHandler.getAllEventsForDay(today).get(0).getTitle();
            txtDayevent.setText(ss);
            txtDayevent.setText(ss);
        } else {
            txtDayevent.setText("فاقد رویداد ");
        }

        monthandyear.setText(calendarHandler.formatNumber(calendarHandler.getMonthName(today)));

        calendarHandler.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onChanged(PersianDate persianDate) {
                // onHeader change by swipe
                monthandyear.setText(calendarHandler.formatNumber(calendarHandler.getMonthName(persianDate)));
            }
        });

        calendarHandler.setOnDayClickedListener(new OnDayClickedListener() {
            @Override
            public void onClick(PersianDate persianDate) {
                //   for (CalendarEvent e : calendarHandler.getEventsTitle(persianDate,true)) {
                String s = calendarHandler.getEventsTitle(persianDate,true);

                if (!calendarHandler.getAllEventsForDay(persianDate).isEmpty()) {
                    String ss = calendarHandler.getAllEventsForDay(persianDate).get(0).getTitle();
                    txtDayevent.setText(ss);
                } else {
                    txtDayevent.setText("فاقد رویداد ");
                }

                String strdmy = calendarHandler.getWeekDayName(persianDate) + " " + calendarHandler.formatNumber(persianDate.getDayOfMonth())
                        + " " + calendarHandler.getMonthName(persianDate) + " " + "سال " + calendarHandler.formatNumber(persianDate.getYear());

                todayImageView.setText(calendarHandler.formatNumber(persianDate.getDayOfMonth()));

                noteDayOfMonth = String.valueOf(persianDate.getDayOfMonth());
                noteMonth = String.valueOf(persianDate.getMonth());
                noteYear = String.valueOf(persianDate.getYear());
                totalDay = noteYear + "-" + noteMonth + "-" + noteDayOfMonth;

                initNote(totalDay);
            }
        });

        calendarHandler.setOnDayLongClickedListener(new OnDayLongClickedListener() {
            @Override
            public void onLongClick(final PersianDate persianDate) {
                Intent intent2 = DetailPagerActivity.newIntent(getActivity(),null);
                intent2.putExtra("dayOfYear", totalDay);
                startActivity(intent2);
            }
        });

        todayImageView.setText(calendarHandler.formatNumber(today.getDayOfMonth()));

        leftMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.goToPreviousMonth();
            }
        });

        rightMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.goToNextMonth();
            }
        });

        mGoToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthandyear.setText(calendarHandler.getMonthName(today));
                pd.update(); // it returns the calendar view without // bt it has bug on rotation >> Reset
                todayImageView.setText(calendarHandler.formatNumber(today.getDayOfMonth()));

                if (!calendarHandler.getAllEventsForDay(today).isEmpty()) {
                    String ss = calendarHandler.getAllEventsForDay(today).get(0).getTitle();
                    txtDayevent.setText(ss);
                } else {
                    txtDayevent.setText("فاقد رویداد ");
                }
            }
        });

        return view;
    }

    public void initNote(String selectedDay) {
        mDetail = DetailLab.get(getActivity()).getDayDetail(selectedDay);

        if (mDetail != null) {
            notePreviewTitle.setText(mDetail.getTitle());
            notePreviewDes.setText(mDetail.getDes());

            noteCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentId = mDetail.getId();

                    Intent intent0 = DetailPagerActivity.newIntent(getActivity(),  mDetail.getId());
                    startActivity(intent0);
                }
            });
        }
        else {
            notePreviewTitle.setText("برای افزودن یادداشت به این روز تقویم کلیک کنید");
            notePreviewDes.setText("");

            noteCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = DetailPagerActivity.newIntent(getActivity(),null);
                    intent1.putExtra("dayOfYear", totalDay);
                    startActivity(intent1);
                }
            });
        }
    }

    /*public void initToolbar() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar();
        centerTitle.setText("تقویم");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.icon_back_smaller_light);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (totalDay != null)
            initNote(totalDay);
        else {
            noteDayOfMonth = String.valueOf(calendarHandler.getToday().getDayOfMonth());
            noteMonth = String.valueOf(calendarHandler.getToday().getMonth());
            noteYear = String.valueOf(calendarHandler.getToday().getYear());
            totalDay = noteYear + "-" + noteMonth + "-" + noteDayOfMonth;

            initNote(totalDay);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
